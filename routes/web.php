<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});


Auth::routes();


Route::get('/home', 'SearchController@index')->name('home');
Route::get('/search/{query}', 'SearchController@search')->name('search');
Route::get('/search/{query}/lazyload/{skip}', 'SearchController@lazyload')->name('lazyload');
Route::get('/profile/{id}', 'DashboardController@index')->name('dashboard');

Route::group(['middleware' => ['auth']], function() {

    Route::get('/classroom/{ch}', 'ClassroomController@index')->name('classroom');
    Route::get('/classroom/overview/{ch}', 'ClassroomController@overview')->name('overview');
    Route::get('/classroom/notification', 'ClassroomController@notification')->name('notification');

    Route::post('/classroom/request-join', 'ClassroomController@requestJoin')->name('requestJoin');
    Route::post('/classroom/approveJoin','ClassroomController@approveJoin')->name('approveJoin');
    Route::post('/classroom/rejectJoin','ClassroomController@rejectJoin')->name('rejectJoin');
    Route::post('/classroom/kickMember','ClassroomController@kickMember')->name('kickMember');

    Route::get('/classroom/create', 'ClassroomController@create')->name('createClassroom');
    Route::post('/classroom/store', 'ClassroomController@store')->name('storeClassroom');
    Route::get('/classroom/delete', 'ClassroomController@delete')->name('deleteClassroom');

    Route::post('/message/emit-slider-event', 'ClassroomController@emitSliderEvent')->name('emitSliderEvent');
    Route::post('/message/emit-chat-message', 'ClassroomController@emitChatMessage')->name('emitChatMessage');
    Route::post('/message/emit-chalk-message', 'ClassroomController@emitChalkMessage')->name('emitChalkMessage');
    Route::post('/message/emit-eraser-message', 'ClassroomController@emitEraserMessage')->name('emitEraserMessage');
});
