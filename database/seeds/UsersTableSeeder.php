<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Randy',
                'email' => 'randy@gmail.com',
                'password' => bcrypt('123456789'),
            ],
            [
                'name' => 'Gary',
                'email' => 'gary@gmail.com',
                'password' => bcrypt('123456789'),
            ],
            [
                'name' => 'Calvine',
                'email' => 'calvine@gmail.com',
                'password' => bcrypt('123456789'),
            ],
            [
                'name' => 'Hendra',
                'email' => 'hendra@gmail.com',
                'password' => bcrypt('123456789'),
            ],
            [
                'name' => 'Rudy',
                'email' => 'rudy@gmail.com',
                'password' => bcrypt('123456789'),
            ],
            [
                'name' => 'Alvin',
                'email' => 'alvin@gmail.com',
                'password' => bcrypt('123456789'),
            ],
        ]);
    }
}
