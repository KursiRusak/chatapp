<?php

use Illuminate\Database\Seeder;

class ClassroomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        DB::table('classrooms')->insert([
            [
                'mentor' => 1,
                'title' => 'Pemrograman Berorientasi Objek Dasar (C++)',
                'slug' => str_slug('Pemrograman Berorientasi Objek Dasar (C++)', '-'),
                'description' => 'merupakan paradigma pemrograman berdasarkan konsep "objek", yang dapat berisi data, dalam bentuk field atau dikenal juga sebagai atribut; serta kode, dalam bentuk fungsi/prosedur atau dikenal juga sebagai method. Semua data dan fungsi di dalam paradigma ini dibungkus dalam kelas-kelas atau objek-objek. Bandingkan dengan logika pemrograman terstruktur. Setiap objek dapat menerima pesan, memproses data, dan mengirim pesan ke objek lainnya, Kelas ini akan menggunakan bahasa C++ sebagai contoh'
            ],
            [
                'mentor' => 1,
                'title' => 'Matematika Dasar 1',
                'slug' => str_slug('Matematika Dasar 1', '-'),
                'description' => 'Mengajarkan dasar-dasar matematika, mencakup logika matematika, integral dan turunan'
            ],
            [
                'mentor' => 1,
                'title' => 'Pemrograman Berorientasi Objek Dasar (Java)',
                'slug' => str_slug('Pemrograman Berorientasi Objek Dasar (Java)', '-'),
                'description' => 'Algoritma pemrograman merupakan paradigma pemrograman berdasarkan konsep "objek", yang dapat berisi data, dalam bentuk field atau dikenal juga sebagai atribut; serta kode, dalam bentuk fungsi/prosedur atau dikenal juga sebagai method. Semua data dan fungsi di dalam paradigma ini dibungkus dalam kelas-kelas atau objek-objek. Bandingkan dengan logika pemrograman terstruktur. Setiap objek dapat menerima pesan, memproses data, dan mengirim pesan ke objek lainnya, Kelas ini akan menggunakan bahasa Java sebagai contoh.'
            ],
            [
                'mentor' => 1,
                'title' => 'Statistika Dasar',
                'slug' => str_slug('Statistika Dasar', '-'),
                'description' => 'Materi dasar matematika, mengajarkan mulai dari membedakan jenis data, memahami jenisnya seperti nominal, ordinal, interval, dan rasio. Menghitung rata-rata, mean, median, modus.'
            ],
            [
                'mentor' => 1,
                'title' => 'Statistika Dasar 2',
                'slug' => str_slug('Statistika Dasar 2', '-'),
                'description' => 'Materi dasar statistik matematika, mengajarkan macam-macam distribusi dalam statistik. Mencakup distribusi Binomial, distribusi Poisson, distribusi Gaussian.'
            ],
            [
                'mentor' => 1,
                'title' => 'Pemrograman Berorientasi Objek Dasar (C#)',
                'slug' => str_slug('Pemrograman Berorientasi Objek Dasar (C#)', '-'),
                'description' => 'Kelas ini merupakan kelas dasar pemrograman. AP Merupakan paradigma pemrograman berdasarkan konsep "objek", yang dapat berisi data, dalam bentuk field atau dikenal juga sebagai atribut; serta kode, dalam bentuk fungsi/prosedur atau dikenal juga sebagai method. Semua data dan fungsi di dalam paradigma ini dibungkus dalam kelas-kelas atau objek-objek. Bandingkan dengan logika pemrograman terstruktur. Setiap objek dapat menerima pesan, memproses data, dan mengirim pesan ke objek lainnya, Kelas ini akan menggunakan bahasa C# sebagai contoh'
            ],
            [
                'mentor' => 1,
                'title' => 'Dasar Rekayasa Sistem Informasi (RSI)',
                'slug' => str_slug('Dasar Rekayasa Sistem Informasi (RSI)', '-'),
                'description' => 'Kelas ini merupakan kelas dasar dari rekayasa sistem informasi, mendalami konsep desain sistem, pembuatan diagram seperti Entity Relationship Diagram (ERD), Data Flow Diagram (DFD), dan Activity Diagram.'
            ],
            [
                'mentor' => 1,
                'title' => 'Algoritma pemrograman (C#)',
                'slug' => str_slug('Algoritma pemrograman (C#)', '-'),
                'description' => 'Kelas ini mengajarkan ilmu wajib bagi semua programer, karena dasar dari ilmu ini merupakan dasar menulis sebuah kode program. kelas ini akan memberikan pemahaman dasar alur logika, pembuatan flowchart sederhana, pembuatan variable, array, multi-dimensional array, if, loop. nested loop.'
            ],
            [
                'mentor' => 1,
                'title' => 'Sistem Operasi Arsitektur Dasar',
                'slug' => str_slug('Sistem Operasi Arsitektur Dasar', '-'),
                'description' => 'Kelas ini akan mencakup dasar2 sistem operasi arsitektur. hasil akhir dari kelas ini yaitu setiap user mampu membuat REST API.'
            ],
            [
                'mentor' => 1,
                'title' => 'Algoritma pemrograman (C++)',
                'slug' => str_slug('Algoritma pemrograman (C++)', '-'),
                'description' => 'Kelas ini mengajarkan ilmu wajib bagi semua programer, karena dasar dari ilmu ini merupakan dasar menulis sebuah kode program. kelas ini akan memberikan pemahaman dasar alur logika, pembuatan flowchart sederhana, pembuatan variable, array, multi-dimensional array, if, loop. nested loop. kelas ini akan menggunakan C++ sebagai contoh.'
            ],
            [
                'mentor' => 1,
                'title' => 'Basic Laravel Framework 5',
                'slug' => str_slug('Basic Laravel Framework 5', '-'),
                'description' => 'Ilmu dasar laravel, konsep MVC, memahami package manager Composer, setiap struktur folder laravel, artisan command, migration, authentication, instalasi fresh project, menulis simple CRUD application.'
            ],
            [
                'mentor' => 1,
                'title' => 'Intermediate Laravel Framework 5',
                'slug' => str_slug('Intermediate Laravel Framework 5', '-'),
                'description' => 'Ilmu menengah laravel, mencakup bundler webpack (laravel mix), storage, middleware, caching menggunakan redis.'
            ],
            [
                'mentor' => 1,
                'title' => 'Advance Laravel Framework 5',
                'slug' => str_slug('Advance Laravel Framework 5', '-'),
                'description' => 'Ilmu lanjutan untuk laravel web framework, mencakup unit test, queue, laravel horizon, event broadcasting, integrasi laravel scout dengan algolia search engine atau dengan elastic search engine (custom driver).'.$faker->sentence($nbWords = 20, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'Basic Laravel-Vuejs',
                'slug' => str_slug('Basic Laravel-Vuejs', '-'),
                'description' => 'Kelas ini merupakan kelas pengenalan untuk integrasi laravel dan vue-js, bagaimana peranan laravel sebagai backend dan vue sebagai frontend.'
            ],
            [
                'mentor' => 1,
                'title' => 'Basic Laravel Lumen',
                'slug' => str_slug('Basic Laravel Lumen', '-'),
                'description' => 'Kelas dasar Lumen Framework. Lumen merupakan micro framework dari laravel. framework ini akan bekerja sebagai backend web.'
            ],
            [
                'mentor' => 1,
                'title' => 'Ruby Programming Language',
                'slug' => str_slug('Ruby Programming Language', '-'),
                'description' => 'Kelas dasar untuk bahasa pemrograman ruby. termasuk package manager ruby gem'
            ],
            [
                'mentor' => 1,
                'title' => 'Ruby on Rails Basic',
                'slug' => str_slug('Ruby on Rails Basic', '-'),
                'description' => 'Ruby on Rails, or Rails, is a server-side web application framework written in Ruby under the MIT License. Rails is a model–view–controller framework, providing default structures for a database, a web service, and web pages.'
            ],
            [
                'mentor' => 1,
                'title' => 'Basic PHP MYSQL for building website',
                'slug' => str_slug('Basic PHP MYSQL for building website', '-'),
                'description' => 'PHP basic tutorial, includes GET POST Request, Sessions, And Integrating to MYSQL'
            ],
            [
                'mentor' => 1,
                'title' => 'JQuery Open Class',
                'slug' => str_slug('JQuery Open Class', '-'),
                'description' => 'jQuery is a cross-platform JavaScript library designed to simplify the client-side scripting of HTML. It is free, open-source software using the permissive MIT License. this class will teach you how to bind an event by creating an event handler'
            ],
            [
                'mentor' => 1,
                'title' => 'The Art of Vanilla JS',
                'slug' => str_slug('The Art of Vanilla JS', '-'),
                'description' => 'This class will show you the power of pure javascrip or as known as vanilla JS. this class will be discusing about basic of javascript API'
            ],
            [
                'mentor' => 1,
                'title' => 'Lorem ipsum',
                'slug' => str_slug('Lorem ipsum', '-'),
                'description' => $faker->sentence($nbWords = 25, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'Algebra for dummies',
                'slug' => str_slug('Algebra for dummies', '-'),
                'description' => $faker->sentence($nbWords = 40, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'English (Elementary Student)',
                'slug' => str_slug('English (Elementary Student)', '-'),
                'description' => $faker->sentence($nbWords = 33, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'Ilmu Alamiah Dasar',
                'slug' => str_slug('Ilmu Alamiah Dasar', '-'),
                'description' => $faker->sentence($nbWords = 38, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'UI/UX prototyping',
                'slug' => str_slug('UI/UX prototyping', '-'),
                'description' => $faker->sentence($nbWords = 42, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'UI/UX prototyping 2',
                'slug' => str_slug('UI/UX prototyping 2', '-'),
                'description' => $faker->sentence($nbWords = 32, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'Knockout JS Tutorial',
                'slug' => str_slug('Knockout JS Tutorial', '-'),
                'description' => $faker->sentence($nbWords = 28, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'Basic Code Igniter',
                'slug' => str_slug('Basic Code Igniter', '-'),
                'description' => $faker->sentence($nbWords = 36, $variableNbWords = true)
            ],
            [
                'mentor' => 1,
                'title' => 'Yii Web Framework',
                'slug' => str_slug('Yii Web Framework', '-'),
                'description' => $faker->sentence($nbWords = 30, $variableNbWords = true)
            ]
        ]);
    }
}
