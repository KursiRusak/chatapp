
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'
window.io = require('socket.io-client');
// window.Pusher = require('pusher-js');


if (typeof io !== 'undefined') {
    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname + ':6001'
    });
}

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

// import Bricks from 'bricks.js'

// const sizes = [
//     { columns: 2, gutter: 10 }, // assumed to be mobile, because of the missing mq property
//     { mq: '768px', columns: 3, gutter: 25 },
//     { mq: '1024px', columns: 4, gutter: 50 }
// ]

// window.instance = Bricks({
//     container: '.brick-container',
//     packed:    'data-packed', // if not prefixed with 'data-', it will be added
//     sizes:     sizes
// })

// instance.pack()

// instance
//     .on('pack',   () => console.log('ALL grid items packed.'))
//     .on('update', () => console.log('NEW grid items packed.'))
//     .on('resize', size => console.log('The grid has be re-packed to accommodate a new BREAKPOINT.'))

// document.addEventListener('DOMContentLoaded', event => {
//     instance
//         .resize(true)     // bind resize handler
//         .pack()           // pack initial items
// })