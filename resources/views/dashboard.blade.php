@extends('layouts.app')

@push('styles')
<style>

#overview-container{
  /* padding-left: 480px;
  padding-right: 480px; */
  text-align:center;
  padding-bottom: 20px;
}
.wrapper{
  width: 1100px;
  margin: 0 auto;
}
.brick-container{
    position: relative;
    width: 960px;
    margin-right: auto;
    margin-left: auto;
}
.classroom-item{
    width: 365px;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 3px;
    background-color: #fff;
    margin: 0;
    padding: 0;
    padding-bottom:
    border: none;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    -webkit-transition: border-color 0.4s; /* Safari */
    transition: border-color 0.4s;
    text-align: left;
}
h5.card-title{
    font-weight: bold;
}
#overview-picture{
  margin-top: -80px;
}
#overview-container h4{
  margin-top: 15px !important;
  font-size: 1.8rem;
}
</style>
@endpush

@section('content')
<div class="wrapper">
  <div class="full-width" id="overview-banner">
      <img class="card-img-top" src="https://source.unsplash.com/1300x400/?{{ $photo }}" width="100%">
  </div>
  <div class="card" id="overview-container">
    <div id="overview-picture">
      <img src="https://api.adorable.io/avatars/200/{{ Auth::user()->email }}" width="200px" height="200px">
    </div>
    <h4>{{ $user->name }}</h4>
    <b>{{ count($subclassrooms) }} SUBSCRIBED &nbsp;&nbsp;&nbsp;&nbsp;{{ count($openclassrooms) }} ROOM</b>
    <div style="width: 600px; margin: 0 auto">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tellus mauris, eleifend vitae sapien vitae, egestas congue ligula. Aliquam pellentesque.</p>
    </div>
    <div class="container" id="home-page" style="margin-top: 20px">
      <div class="row nm">
        <div class="col-md-12" style="margin-bottom: 35px">
          <h5><b>SUBSCRIBED ROOM</b></h5>
          <hr>
          <div class="subclassroom-container">
            <div class="row">
              @foreach($subclassrooms as $key => $subclassroom)
              <div class="card classroom-item col-md-4">
                  <a href="{{ route('overview', ['ch' => $subclassroom->classroom->slug]) }}">
                      <img class="card-img-top" src="https://source.unsplash.com/365x100/?{{ $photos[$key%14] }}" alt="{{ $subclassroom->classroom->title }}" width="365px" height="100px">
                  </a>
                  <a href="{{ route('overview', ['ch' => $subclassroom->classroom->slug]) }}">
                      <div class="card-body">
                          <h5 class="card-title">{{ $subclassroom->classroom->title }}</h5>
                          <p class="card-text">{{ substr($subclassroom->classroom->description, 0, 120) }}{{ strlen($subclassroom->classroom->description) > 120 ? '...' : '' }}</p>
                      </div>
                  </a>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <h5><b>OPEN ROOM</b></h5>
          <hr>
          <div class="openclassroom-container">
            <div class="row">
              @foreach($openclassrooms as $key => $classroom)
              <div class="card classroom-item col-md-4">
                  <a href="{{ route('overview', ['ch' => $classroom->slug]) }}">
                      <img class="card-img-top" src="https://source.unsplash.com/365x100/?{{ $photos[$key%14] }}" alt="{{ $classroom->title }}" width="365px" height="100px">
                  </a>
                  <a href="{{ route('overview', ['ch' => $classroom->slug]) }}">
                      <div class="card-body">
                          <h5 class="card-title">{{ $classroom->title }}</h5>
                          <p class="card-text">{{ substr($classroom->description, 0, 120) }}{{ strlen($classroom->description) > 120 ? '...' : '' }}</p>
                      </div>
                  </a>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
