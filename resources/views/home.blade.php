@extends('layouts.app')

@push('styles')
<style>
#home-page{
    padding-bottom: 100px;
}
#aa-input-container, .algolia-autocomplete, #aa-search-input, .aa-dropdown-menu{
    width: 100%;
}
.aa-input-container {
  display: inline-block;
  position: relative;
}
.aa-input-search {
  width: 100%;
  border: 1px solid rgba(228, 228, 228, 0.6);
  padding: 12px 28px 12px 12px;
  box-sizing: border-box;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
.aa-input-search::-webkit-search-decoration, .aa-input-search::-webkit-search-cancel-button, .aa-input-search::-webkit-search-results-button, .aa-input-search::-webkit-search-results-decoration {
    display: none;
}
.aa-input-icon {
  height: 16px;
  width: 16px;
  position: absolute;
  top: 50%;
  right: 16px;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  fill: #e4e4e4;
}
.aa-dropdown-menu {
  background-color: #fff;
  border: 1px solid rgba(228, 228, 228, 0.6);
  min-width: 300px;
  margin-top: 0px;
  box-sizing: border-box;
}
.aa-suggestion {
  padding: 12px;
  cursor: pointer;
}
.aa-suggestion + .aa-suggestion {
    border-top: 1px solid rgba(228, 228, 228, 0.6);
}
.aa-suggestion:hover, .aa-suggestion.aa-cursor {
    background-color: rgba(241, 241, 241, 0.35);
}
em{
    font-style: normal;
    font-weight:bold;
}
.brick-container{
    position: relative;
    width: 960px;
    margin-right: auto;
    margin-left: auto;
}
.classroom-item{
    width: 365px;
    border: 1px solid rgba(0, 0, 0, 0.125);
    border-radius: 3px;
    background-color: #fff;
    margin: 0;
    padding: 0;
    padding-bottom:
    border: none;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    -webkit-transition: border-color 0.4s; /* Safari */
    transition: border-color 0.4s;
    visibility: hidden;
}
h5.card-title{
    font-weight: bold;
}
#new-classroom{
  position: fixed;
  bottom: 25px;
  right: 25px;
  width: 50px;
  height: 50px;
  border: 1px solid transparent;
  border-radius: 25px;
}
</style>
@endpush

@section('content')
<div class="container" id="home-page">
    <div class="row">
        <div class="col-md-12" id="active-user-panel">
            <div class="card">
                <div class="card-body">
                    <div class="aa-input-container" id="aa-input-container">
                        <form id="search-classroom-form" method="GET" action="{{ route('search', ['query' => ':_q']) }}">
                            <input type="search" id="aa-search-input" class="aa-input-search" placeholder="Search for classroom..." name="q" autocomplete="off" />
                            <svg class="aa-input-icon" viewBox="654 -372 1664 1664">
                                <path d="M1806,332c0-123.3-43.8-228.8-131.5-316.5C1586.8-72.2,1481.3-116,1358-116s-228.8,43.8-316.5,131.5  C953.8,103.2,910,208.7,910,332s43.8,228.8,131.5,316.5C1129.2,736.2,1234.7,780,1358,780s228.8-43.8,316.5-131.5  C1762.2,560.8,1806,455.3,1806,332z M2318,1164c0,34.7-12.7,64.7-38,90s-55.3,38-90,38c-36,0-66-12.7-90-38l-343-342  c-119.3,82.7-252.3,124-399,124c-95.3,0-186.5-18.5-273.5-55.5s-162-87-225-150s-113-138-150-225S654,427.3,654,332  s18.5-186.5,55.5-273.5s87-162,150-225s138-113,225-150S1262.7-372,1358-372s186.5,18.5,273.5,55.5s162,87,225,150s113,138,150,225  S2062,236.7,2062,332c0,146.7-41.3,279.7-124,399l343,343C2305.7,1098.7,2318,1128.7,2318,1164z" />
                            </svg>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row nm">
        <div class="brick-container">
            @foreach($classrooms as $key => $classroom)
            <div class="card classroom-item" onmouseover="MouseOver(this);" onmouseout="MouseOut(this);">
                <a href="{{ route('overview', ['ch' => $classroom->slug]) }}">
                    <img class="card-img-top" src="https://source.unsplash.com/365x100/?{{ $photos[$key] }}" alt="{{ $classroom->title }}" width="365px" height="100px">
                </a>
                <a href="{{ route('overview', ['ch' => $classroom->slug]) }}">
                    <div class="card-body">
                        <h5 class="card-title">{{ $classroom->title }}</h5>
                        <p class="card-text">{{ substr($classroom->description, 0, rand(200, 600)) }}</p>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
    <button id="new-classroom" class="btn btn-primary" data-toggle="modal" data-target="#new-classroom-modal">
      <i class="fa fa-plus"></i>
    </button>
</div>

<div id="new-classroom-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Room</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="new-post-form" method="post" action="{{ route('storeClassroom') }}" enctype="multipart/form-data">
                <div class="modal-body">
                        <div class="form-group">
                            <label><b>Title</b></label>
                            <input type="text" class="form-control" name="title" required>
                        </div>
                        <div class="form-group">
                            <label><b>Summary</b></label>
                            <textarea class="form-control" rows="5" name="description" required></textarea>
                        </div>
                        <div class="form-group">
                            <label><b>PDF</b></label>
                            <input type="file" class="form-control" name="pdf">
                        </div>
                        {{ csrf_field() }}
                </div>
                <div class="modal-footer">
                      <button id="share-post" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>

    var skip = 0;
    var state = 'idle'; // idle, processing, completed
    var activeQuery = '{{ (isset($query))? $query : "all" }}'

    function MouseOver(elem) {
        elem.style.borderColor = "#6f42c1";
    }

    function MouseOut(elem) {
        elem.style.borderColor = "rgba(0, 0, 0, 0.125)";
    }

    document.getElementById('aa-search-input').onkeydown = function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            var url = $('#search-classroom-form').prop('action');
            url = url.replace(':_q', $('#aa-search-input').val());
            window.location.href=url;
        }
    };

    var client = algoliasearch('25JKNNY7IG', 'dfa316a09b7ccda8d722d93268cc25dd')
    var index = client.initIndex('classrooms');
    autocomplete('#aa-search-input',
    { hint: false }, {
        source: autocomplete.sources.hits(index, {hitsPerPage: 5}),
        //value to be displayed in input control after user's suggestion selection
        displayKey: 'title',
        //hash of templates used when rendering dataset
        templates: {
            //'suggestion' templating function used to render a single suggestion
            suggestion: function(suggestion) {
                console.log(suggestion);
                return '<span>' + suggestion._highlightResult.title.value + '</span>';
            }
        }
    });

    const sizes = [
        { columns: 2, gutter: 10 }, // assumed to be mobile, because of the missing mq property
        { mq: '768px', columns: 3, gutter: 25 },
        { mq: '1024px', columns: 3, gutter: 10 }
    ]

    const instance = Bricks({
        container: '.brick-container',
        packed:    'data-packed', // if not prefixed with 'data-', it will be added
        sizes:     sizes
    })

    window.sr = ScrollReveal();

    instance
        .on('pack',   () => {
            console.log('ALL grid items packed.');
            $('.classroom-item').css({'visibility': 'visible'});
            sr.reveal('.classroom-item')
        })
        .on('update', () => {
            $('.classroom-item').css({'visibility': 'visible'});
            sr.reveal('.classroom-item')
        })
        .on('resize', size => console.log('The grid has be re-packed to accommodate a new BREAKPOINT.'))


    document.addEventListener('DOMContentLoaded', event => {
        instance
            .resize(true)     // bind resize handler
            .pack()           // pack initial items
    })

    function getClassroom()
    {
        state = 'processing'
        url = '{{ route("lazyload", ["query" => ":_q", "skip" => ":_s"]) }}'
        url = url.replace(':_q', activeQuery)
        url = url.replace(':_s', skip.toString())
        return $.ajax({
            url: url,
            type: 'GET'
        });
    }

    function lazyload(){
        if(($(window).scrollTop() + $(window).height()) > $(document).height() - 100){
            if(state != 'processing' && state != 'completed'){
                getClassroom().then((resp) => {
                    console.log(resp)
                    skip += 1;
                    if(resp.length < 15)
                        state = 'completed'
                    else
                        state = 'idle'

                    resp.classrooms.forEach((data, key) => {
                        $('.brick-container').append('\
                            <div class="card classroom-item" onmouseover="MouseOver(this);" onmouseout="MouseOut(this);" style="visibility:hidden">\
                                <a href="classroom/'+ data.slug +'">\
                                    <img class="card-img-top" src="https://source.unsplash.com/365x100/?'+ resp.photos[key] +'" alt="'+ data.title +'" width="365px" height="100px">\
                                </a>\
                                <a href="classroom/'+ data.slug +'">\
                                    <div class="card-body">\
                                        <h5 class="card-title">'+ data.title +'</h5>\
                                        <p class="card-text">'+ data.description +'</p>\
                                    </div>\
                                </a>\
                            </div>\
                        ');
                    });

                    instance.update();

                })
            }
        }
    }

    $(document).ready(function(){
        $(window).scroll(lazyload);
    })



</script>
@endpush
