<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ajarin</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset(mix('css/all.css')) }}" rel="stylesheet">
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    <style>
        body{
            color: rgb(0,0,0,0.7);
            font-family: 'Roboto', sans-serif;
        }
        #app{
            padding-top: 100px;
        }
        a:hover, a:visited, a:link, a:active {
            color: rgb(0,0,0,0.7);
            text-decoration: none!important;
            -webkit-box-shadow: none!important;
            box-shadow: none!important;
        }
        #header{
            position:absolute;
            top:0;
            left:0;
            height: 100px;
            width:100%;
            z-index:2;
            -webkit-transition: color .2s linear, background-color .2s linear, height .1s linear;
            -moz-transition: color .2s linear, background-color .2s linear, height .1s linear;
            -o-transition: color .2s linear, background-color .2s linear, height .1s linear;
            transition: color .2s linear, background-color .2s linear, height .1s linear;
        }
        #logo{
            font-weight: bold;
            font-size: 1.775rem;
        }
        #black-box{
            padding-right: 2px;
            background-color:rgb(0,0,0,0.7);
            border: 3px;
            width:35px;
            height:35px;
            color:white;
            display:inline-block;
            text-align:right;
            border-radius: 3px;
            -webkit-transition: color .2s linear, background-color .2s linear;
            -moz-transition: color .2s linear, background-color .2s linear;
            -o-transition: color .2s linear, background-color .2s linear;
            transition: color .2s linear, background-color .2s linear;
        }
        #navbarDropdown{
          display: inline;
        }
    </style>
    @stack('styles')
</head>
<body>
    <div id="app">
        <nav id="header" class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a id="logo" href="{{ route('home') }}">
                    <div id="logo">
                        <div id="black-box">a</div>jarin
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <img src="https://api.adorable.io/avatars/50/{{ Auth::user()->email }}" width="25px" height="25px">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('dashboard', ['id' => Auth::user()->id]) }}">
                                        Profile
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <!-- Scripts -->
    <script src="{{ asset(mix('js/app.js')) }}"></script>
    <script src="{{ asset(mix('js/all.js')) }}"></script>
    <script>
        $(window).scroll(function() {
            if ( $(window).scrollTop() >= 2 ) {
                $('#header').css({
                    'position': 'fixed',
                    'height': '60px',
                    // 'backgroundColor': '#6f42c1',
                    // 'color': '#fff'
                });
                // $('#navbarSupportedContent a').css('color', '#fff');
                $('#black-box').css({
                    // 'backgroundColor': '#fff',
                    // 'color': '#6f42c1',
                });
            } else {
                $('#header').css({
                    'position': 'absolute',
                    'height': '100px',
                    // 'backgroundColor': '#fff',
                    // 'color': 'rgb(0,0,0,0.7)'
                });
                // $('#navbarSupportedContent a').css('color', 'rgb(0,0,0,0.7)');
                $('#black-box').css({
                    // 'backgroundColor': 'rgb(0,0,0,0.7)',
                    // 'color': '#fff',
                });
            }
        });
    </script>
    @stack('scripts')
</body>
</html>
