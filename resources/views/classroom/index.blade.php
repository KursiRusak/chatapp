@extends('layouts.app')

@push('styles')
<link rel="stylesheet" href="{{ asset('css/chalk.css') }}">
<style>
#chalkboard-box, #chalkboard{
    height:600px;
}
#header-card{
    padding: 10px 15px;
    display: -webkit-box;
    display: -ms-flexbox;
}
#room-title{
    margin: 0;
    font-size: 17px;
    font-weight: bold;
    display: inline;
}
#setting-button{
    display:inline;
    width:120px;
}
#send-button{
    height: 60px;
    border-radius: 30px;
    width: 60px;
}
#convo-box{
    min-height: 50px;
    width: 100%;
    overflow-y: scroll;
}
#chat-textarea{
    height: 60px;
}
#chat-panel .card-body{
    padding:0;
}
#message-form-box{
    padding:10px;
    background-color: #eee;
}
#message-form-box .col-10{
    padding-left:16px;
    padding-right: 5px;
    flex-basis: 88%;
    max-width: 88%;
}
#message-form-box .col-2{
    padding-left:8px;
    padding-right:16px;
    flex-basis: 10%;
    max-width: 10%;
}
#convo-box{
    width: 100%;
    height: 200px;
    padding:10px;
}
.max-width-component{
    width:1140px;
    margin:0 auto;
    position:relative;
}
#settings-modal .modal-body hr{
    margin-top: 5px;
    margin-bottom: 5px;
}
#settings-modal .modal-body label{
    margin-bottom: 0px;
}
#member-block-content{
  max-height: 180px;
  overflow-y: scroll;
}
#request-block-content{
  max-height: 130px;
  overflow-y: scroll;
}
</style>
@endpush

@section('content')
<div id="chat-page">
    <div class="container">
        <!-- <div class="row justify-content-center"> -->
        <div class="card col-md-12" id="header-card">
            <span id="room-title">{{ $classroom->title }}</span>
            @if($settings)
            <button id="setting-button" class="btn btn-sm btn-default pull-right" data-toggle="modal" data-target="#settings-modal" ><i class="fa fa-cog"></i> Settings</button>
            @endif
        </div>
    </div>
    <br>
    <div class="max-width-component">
        <div class="col-md-12">

            <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div id="chalkboard-box"></div>
                    </div>
                    @if($classroom->pdf)
                    <div class="carousel-item">
                      <!-- <embed src="http://www.pdf995.com/samples/pdf.pdf" width="500" height="375"> -->
                        <object data="{{ asset('pdf/'.$classroom->pdf) }}" type="application/pdf" width="1140px" height="614px">
                            <p>Alternative text - include a link <a href="http://www.pdf995.com/samples/pdf.pdf">to the PDF!</a></p>
                        </object>
                        <!-- <iframe src="http://docs.google.com/gview?url={{ asset('uploads/'.$classroom->pdf) }}&embedded=true" style="width:100%; height:600px;" frameborder="0"></iframe> -->
                    </div>
                    @endif
                </div>

            </div>



        </div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-3" id="active-user-panel">
                <div class="card">
                    <div class="card-header">User</div>
                    <div class="card-body">
                        <ul></ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9" id="chat-panel">
                <div class="card">
                    <div class="card-header">Chat</div>
                    <div class="card-body">
                        <div id="convo-box"></div>
                        <div id="message-form-box">
                            <form>
                                <div class="row">
                                    <div class="col-10">
                                        <textarea class="form-control" id="chat-textarea"></textarea>
                                    </div>
                                    <div class="col-2">
                                        <button class="btn btn-default form-control" id="send-button"><i class="fa fa-paper-plane fa-1x"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if($settings)
<div id="settings-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Settings</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div id="member-block">
                <label>Member (<span id="refresh-member">{{ count($member) }} </span>)</label>
                <hr>
                <div id="member-block-content">
                  <table id="member-table" style="width: 100%;"></table>
                </div>
              </div>
              <div id="request-block">
                <label>Request Join (<span id="refresh-request">{{ count($requestMember) }}</span>)</label>
                <hr>
                <div id="request-block-content">
                  <table id="request-table" style="width: 100%">
                  </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
@endif
@endsection


@push('scripts')
<!-- <script src="{{ asset('js/chalk.js') }}"></script> -->
<script>
@if($settings)
    let memberList = [];
    let requestList = [];

    <?php foreach ($member as $m): ?>
      memberList.push({user_id: <?= $m->user_id ?>, name: '<?= $m->user->name ?>', image: '<?= "https://api.adorable.io/avatars/50/".$m->user->email ?>'});
    <?php endforeach; ?>

    <?php foreach ($requestMember as $r): ?>
      requestList.push({user_id: <?= $r->user_id ?>, name: '<?= $r->user->name?>', image: '<?= "https://api.adorable.io/avatars/50/".$r->user->email ?>'});
    <?php endforeach; ?>

    function renderModal(){
      $('#member-table').html("");
      memberList.forEach(m => {
        $('#member-table').append(`\
          <tr>\
            <td><img src="${m.image}" width="25px" height="25px"> &nbsp ${m.name}</td>\
            <td align="right">\
              <button class="btn btn-sm btn-danger kick-member-btn" data-id="${m.user_id}">Kick</button>\
            </td>\
          </tr>\
        `);
      });

      $('#request-table').html("");
      requestList.forEach(r => {
        $('#request-table').append(`\
          <tr>\
            <td><img src="${r.image}" width="25px" height="25px"> &nbsp ${r.name}</td>\
            <td align="right">\
              <button class="btn btn-sm btn-success accept-member-btn" data-id="${r.user_id}"><i class="fa fa-check"></i> Accept</button>\
              <button class="btn btn-sm btn-danger reject-member-btn" data-id="${r.user_id}">Reject</button>\
            </td>\
          </tr>\
        `);
      });

      $('#refresh-member').html(
        memberList.length
      );

      $('#refresh-request').html(
        requestList.length
      );
    }
@endif

    // function two(){
    //   renderModal();
    //   refresh();
    // }


    let channel        = 'classroom.{{ $classroom->slug }}';
    var connectedUsers = [];
    var messages       = [];

    $('#chalkboard').remove();
	$('.chalk').remove();
	$('#chalkboard-box').prepend('<div class="panel"><a class="link" target="_blank">Save</a></div>');
	$('#chalkboard-box').prepend('<img src="{{ asset("img/bg.png") }}" id="pattern" width=50 height=75>');
	$('#chalkboard-box').prepend('<canvas id="chalkboard"></canvas>');
	$('#chalkboard-box').prepend('<div class="chalk"></div>');
    var canvas         = document.getElementById("chalkboard");
    $('#chalkboard').css('width',$('#chalkboard-box').width());
	$('#chalkboard').css('height',$('#chalkboard-box').height());
	canvas.width       = $('#chalkboard-box').width();
    canvas.height      = $('#chalkboard-box').height();
    var ctx            = canvas.getContext("2d");
    var xLast = 0;
    var yLast = 0;
    var brushDiameter = 7;
    var eraserWidth = 50;
    var eraserHeight = 100;

    function emitMessage(msg)
    {
        return $.ajax({
            type: 'POST',
            url: '{{ route("emitChatMessage") }}',
            data: {_token: '{{ csrf_token() }}', channel: channel, message: msg}
        });
    }


    function emitChalk(x, y, xLast, yLast)
    {
        return $.ajax({
            type: 'POST',
            url: '{{ route("emitChalkMessage") }}',
            data: {_token: '{{ csrf_token() }}', channel: channel, x: x, y: y, xLast: xLast, yLast: yLast}
        });
    }

    function emitEraser(x, y, xLast, yLast)
    {
        return $.ajax({
            type: 'POST',
            url: '{{ route("emitEraserMessage") }}',
            data: {_token: '{{ csrf_token() }}', channel: channel, x: x, y: y, xLast: xLast, yLast: yLast}
        });
    }

    function updateMemberStatus(user_id)
    {
        return $.ajax({
            type: 'POST',
            url: '{{ route("approveJoin") }}',
            data: {_token: '{{ csrf_token() }}', channel: channel, user_id: user_id}
        });
    }

    function updateRejectMember(user_id)
    {
        return $.ajax({
            type: 'POST',
            url: '{{ route("rejectJoin") }}',
            data: {_token: '{{ csrf_token() }}', channel: channel, user_id: user_id}
        });
    }

    function updateKickMember(user_id)
    {
        return $.ajax({
            type: 'POST',
            url: '{{ route("kickMember") }}',
            data: {_token: '{{ csrf_token() }}', channel: channel, user_id: user_id}
        });
    }

    function onClickSendMessage(e)
    {
        e.preventDefault();
        msg = $('#chat-textarea').val();
        emitMessage(msg).then((resp) => {

        });
        $('#chat-textarea').val('');
    }

    function onKeyDownChatTextArea(e)
    {
        if (e.which === 13) {
            onClickSendMessage(e);
        }
    }


    $('#send-button').click(onClickSendMessage);
    $("#chat-textarea").keydown(onKeyDownChatTextArea);

    function renderActiveUser()
    {
        $('#active-user-panel ul').empty();
        connectedUsers.forEach((user) => {
            $('#active-user-panel ul').append('<li>'+user.name+'</li>')
        })
    }

    function drawOnClient(x, y)
    {
      console.log(x + " " + y);
        ctx.strokeStyle = 'rgba(255,255,255,'+(0.4+Math.random()*0.2)+')';
        ctx.beginPath();
        ctx.moveTo(xLast - 15, yLast + 32);
        ctx.lineTo(x - 15, y + 32);
        ctx.stroke();

        // Chalk Effect
        var length = Math.round(Math.sqrt(Math.pow(x-xLast,2)+Math.pow(y-yLast,2))/(5/brushDiameter));
        var xUnit = (x-xLast)/length;
        var yUnit = (y-yLast)/length;
        for(var i=0; i<length; i++ ){
            var xCurrent = (xLast - 15)+(i*xUnit);
            var yCurrent = (yLast + 32)+(i*yUnit);
            var xRandom = xCurrent+(Math.random()-0.5)*brushDiameter*1.2;
            var yRandom = yCurrent+(Math.random()-0.5)*brushDiameter*1.2;
            ctx.clearRect( xRandom, yRandom, Math.random()*2+2, Math.random()+1);
        }
        // emitChalk(x, y, xLast, yLast);
        // xLast = x;
        // yLast = y;
    }

    function eraseOnClient(x, y)
    {
      ctx.clearRect ((x-15)-0.5*eraserWidth,(y+32)-0.5*eraserHeight,eraserWidth,eraserHeight);
    }

    Echo.join(channel)
        .here((users) => {
            console.log('total user');
            console.log(users);
            connectedUsers = users;
            renderActiveUser();
        })
        .joining((user) => {
            console.log('user joining');
            console.log(user);
            connectedUsers.push(user);
            console.log(connectedUsers);
            renderActiveUser();
        })
        .leaving((user) => {
            console.log('user leaving');
            console.log(user);
            connectedUsers = connectedUsers.filter(connectedUser => connectedUser.id != user.id);
            renderActiveUser();
        })
        .listen('MessagePosted', (payload) => {
            // console.log('message posted');
            // console.log(payload);
            $('#convo-box').append('<span style="display:block"><b>'+payload.user.name+': </b>'+payload.message.body+'</span>');
        })
        .listen('ChalkScratched', (payload) => {
            // console.log('new chalk event broadcasted');
            // console.log(payload);
            xLast = payload.xLast;
            yLast = payload.yLast;
            drawOnClient(payload.x, payload.y);
        })
        .listen('ErasedEvent', (payload) => {
            // console.log('new chalk event broadcasted');
            // console.log(payload);
            xLast = payload.xLast;
            yLast = payload.yLast;
            eraseOnClient(payload.x, payload.y);
        });


    function chalkboard(){
        // $('#chalkboard').remove();
        // $('.chalk').remove();
        // $('#chalkboard-box').prepend('<div class="panel"><a class="link" target="_blank">Save</a></div>');
        // $('#chalkboard-box').prepend('<img src="/img/bg.png" id="pattern" width=50 height=75>');
        // $('#chalkboard-box').prepend('<canvas id="chalkboard"></canvas>');
        // $('#chalkboard-box').prepend('<div class="chalk"></div>');

        // var canvas = document.getElementById("chalkboard");
        // $('#chalkboard').css('width',$('#chalkboard-box').width());
        // $('#chalkboard').css('height',$('#chalkboard-box').height());
        // canvas.width = $('#chalkboard-box').width();
        // canvas.height = $('#chalkboard-box').height();

        // var ctx = canvas.getContext("2d");

        var width = canvas.width;
        var height = canvas.height;
        var mouseX = 0;
        var mouseY = 0;
        var mouseD = false;
        var eraser = false;
        // var xLast = 0;
        // var yLast = 0;
        var brushDiameter = 7;
        var eraserWidth = 50;
        var eraserHeight = 100;
        var offset = $("#chalkboard").offset();

        $('#chalkboard').css('cursor','none');
        document.onselectstart = function(){ return false; };
        ctx.fillStyle = 'rgba(255,255,255,0.5)';
        ctx.strokeStyle = 'rgba(255,255,255,0.5)';
        ctx.lineWidth = brushDiameter;
        ctx.lineCap = 'round';

        var patImg = document.getElementById('pattern');

        document.addEventListener('touchstart', function(evt) {
            //evt.preventDefault();
            var touch = evt.touches[0];
            disableScrolling();
            mouseD = true;
            mouseX = touch.pageX - offset.left;
            mouseY = touch.pageY - offset.top;
            xLast = mouseX;
            yLast = mouseY;
            draw(mouseX+1,mouseY+1);
            // emitChalk(mouseX+1, mouseY+1)//draw(mouseX + 1, mouseY + 1);
        }, false);

        document.addEventListener('touchmove', function(evt) {
            var touch = evt.touches[0];
            mouseX = touch.pageX - offset.left;
            mouseY = touch.pageY - offset.top;
            if (mouseY < height && mouseX < width) {
                evt.preventDefault();
                $('.chalk').css('left', mouseX + 'px');
                $('.chalk').css('top', mouseY + 'px');
                //$('.chalk').css('display', 'none');
                if (mouseD) {
                    draw(mouseX+1,mouseY+1);
                    // emitChalk(mouseX+1, mouseY+1)//draw(mouseX, mouseY);
                }
            }
        }, false);

        document.addEventListener('touchend', function(evt) {
            mouseD = false;
            enableScrolling();
        }, false);

        $('#chalkboard').css('cursor','none');
        ctx.fillStyle = 'rgba(255,255,255,0.5)';
        ctx.strokeStyle = 'rgba(255,255,255,0.5)';
        ctx.lineWidth = brushDiameter;
        ctx.lineCap = 'round';

        $('#chalkboard-box').mousedown(function(evt){
            mouseD = true;
            xLast = mouseX;
            yLast = mouseY;
            if(evt.button == 2){
                erase(mouseX,mouseY);
                eraser = true;
                $('.chalk').addClass('eraser');
            }else{
                if(!$('.panel').is(':hover')){
                    draw(mouseX+1,mouseY+1);
                }
            }
        });

        $('#chalkboard-box').mousemove(function(evt){
            mouseX = evt.pageX - offset.left;
            mouseY = evt.pageY - offset.top;
            if(mouseY<height && mouseX<width){
                $('.chalk').css('left',(mouseX-0.5*brushDiameter)+'px');
                $('.chalk').css('top',(mouseY-0.5*brushDiameter)+'px');
                if(mouseD){
                    if(eraser){
                        erase(mouseX,mouseY);
                    }else{
                        draw(mouseX,mouseY);
                        }
                    }
            }else{
                $('.chalk').css('top',height-10);
            }
        });

        $('#chalkboard-box').mouseup(function(evt){
            mouseD = false;
            if(evt.button == 2){
                eraser = false;
                $('.chalk').removeClass('eraser');
            }
        });

        $('#chalkboard-box').keyup(function(evt){
            if(evt.keyCode == 32){
                ctx.clearRect(0,0,width,height);
                layPattern();
            }
        });



        // $('#chalkboard-box').keyup(function(evt){
        // 	if(evt.keyCode == 83){
        // 		changeLink();
        // 	}
        // });

        document.oncontextmenu = function() {return false;};

        function draw(x,y){
            ctx.strokeStyle = 'rgba(255,255,255,'+(0.4+Math.random()*0.2)+')';
            ctx.beginPath();
            ctx.moveTo(xLast - 15, yLast + 32);
            ctx.lineTo(x - 15, y + 32);
            ctx.stroke();

            // Chalk Effect
            var length = Math.round(Math.sqrt(Math.pow(x-xLast,2)+Math.pow(y-yLast,2))/(5/brushDiameter));
            var xUnit = (x-xLast)/length;
            var yUnit = (y-yLast)/length;
            for(var i=0; i<length; i++ ){
                var xCurrent = (xLast - 15)+(i*xUnit);
                var yCurrent = (yLast + 32)+(i*yUnit);
                var xRandom = xCurrent+(Math.random()-0.5)*brushDiameter*1.2;
                var yRandom = yCurrent+(Math.random()-0.5)*brushDiameter*1.2;
                ctx.clearRect( xRandom, yRandom, Math.random()*2+2, Math.random()+1);
            }
            emitChalk(x, y, xLast, yLast);
            xLast = x;
            yLast = y;
        }

        function erase(x,y){
            ctx.clearRect ((x-15)-0.5*eraserWidth,(y+32)-0.5*eraserHeight,eraserWidth,eraserHeight);

            emitEraser(x, y, xLast, yLast);
        }

        $('.link').click(function(evt){

            $('.download').remove();

            var imgCanvas = document.createElement('canvas');
            var imgCtx = imgCanvas.getContext("2d");
            var pattern = imgCtx.createPattern(patImg,'repeat');

            imgCanvas.width = width;
            imgCanvas.height = height;

            imgCtx.fillStyle = pattern;
            imgCtx.rect(0,0,width,height);
            imgCtx.fill();


            var layimage = new Image;
            layimage.src = canvas.toDataURL("image/png");

            setTimeout(function(){

                imgCtx.drawImage(layimage,0,0);

                var compimage = imgCanvas.toDataURL("image/png");//.replace('image/png','image/octet-stream');

                $('.panel').append('<a href="'+compimage+'" download="chalkboard.png" class="download">Download</a>');
                $('.download').click(function(){
                    IEsave(compimage);
                });

            }, 500);


        });

        function IEsave(ctximage){
            setTimeout(function(){
                var win = window.open();
                $(win.document.body).html('<img src="'+ctximage+'" name="chalkboard.png">');
            },500);
        }

        $(window).resize(function(){
            // chalkboard();
        });

    }

    function acceptMember(){
      id = $(this).data('id');
      // request ajax
      updateMemberStatus(id).then((response) => {
        let temp = requestList.find(elem => {
          return elem.user_id == id;
        });
        requestList = requestList.filter(item => item.user_id !== temp.user_id);
        memberList.push(temp);
        renderModal();
        swal('', 'new member has been added.', 'success')
      })
      // update view
    }

    function rejectMember(){
      id = $(this).data('id');
      // request ajax
      updateRejectMember(id).then((response) => {
        requestList = requestList.filter(item => item.user_id !== id);
        renderModal();
        swal('', 'member has been reject.', 'success')
      })
      // update view
    }

    function kickMember(){
      id = $(this).data('id');
      // request ajax
      updateKickMember(id).then((response) => {
        memberList = memberList.filter(item => item.user_id !== id);
        renderModal();
        swal('', 'member has been kick from room.', 'success')
      })
      // update view
    }

    function disableScrolling(){
        var x=window.scrollX;
        var y=window.scrollY;
        window.onscroll=function(){window.scrollTo(x, y);};
    }

    function enableScrolling(){
        window.onscroll=function(){};
    }

    $(function(){
        $('.flexslider').flexslider({
            slideshow: false
        });
        chalkboard();

    @if($settings)
        renderModal();

        $('#request-table').on("click", '.accept-member-btn', acceptMember);
        $('#request-table').on("click", '.reject-member-btn', rejectMember);
        $('#member-table').on("click", '.kick-member-btn', kickMember);
    @endif
    })

</script>
@endpush
