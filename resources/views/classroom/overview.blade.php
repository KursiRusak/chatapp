@extends('layouts.app')

@push('styles')
<style>
#overview-banner{
  margin-top: -24px;
  margin-bottom: -80px;
  width: 100%;
  height: 350px;
  overflow: hidden;
}
#overview-content{
  padding: 25px;
}
#overview-content h4{
  margin:0;
}
.weak{
  color: rgb(0,0,0,0.5);
  font-size: 12px;
}
.overview-button{
  width: 150px;
  height:50px;
}
#overview-waiting-button{

}
</style>
@endpush

@section('content')
<!-- <div class="container" id="home-page"> -->
<div class="full-width" id="overview-banner">
    <img class="card-img-top" src="https://source.unsplash.com/1300x500/?{{ $photo }}" alt="{{ $classroom->title }}" width="100%">
</div>
<div class="container card" id="overview-content">
  <h4>{{ $classroom->title }}</h4>
  <span class="weak">Mentor - {{ $classroom->mentor_user->name }}</span>
  <br>
  <p>{{ $classroom->description }}</p>
  <div class="button-block">
  @if($status == 'not subscriber')
      <button id="overview-request-button" class="btn btn-primary pull-right overview-button">Request Join</button>
    <!-- </a> -->
  @elseif($status == 'not approved')
    <!-- <a href="{{ route('classroom', ['ch' => $classroom->slug]) }}"> -->
      <button disabled id="overview-waiting-button" class="btn pull-right overview-button">Request Join</button>
    <!-- </a> -->
  @elseif($status == 'subscriber')
    <a href="{{ route('classroom', ['ch' => $classroom->slug]) }}" target="_blank">
      <button id="overview-join-button" class="btn btn-success pull-right overview-button">Join Room</button>
    </a>
  @endif

  </div>
</div>
<!-- </div> -->
@endsection

@push('scripts')
<script>
function request(){
  return $.ajax({
    url: '{{ route("requestJoin") }}',
    type: 'post',
    data: {classroom: '{{ $classroom->id }}', _token: '{{ csrf_token() }}'},
  });
}

function requestBtnHandler(){
  request().then((response) => {
    swal('', 'your request has been sent.', 'success')
    .then(data => {
      window.location.href = "{{ route('home') }}"
    })
  })
}

$(function(){
  $('#overview-request-button').click(requestBtnHandler)
});
</script>
@endpush
