let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .scripts([
        'public/js/algolia-search/algoliasearch.min.js',
        'public/js/autocomplete.js/autocomplete.min.js',
        'node_modules/bricks.js/dist/bricks.js',
        'node_modules/scrollreveal/dist/scrollreveal.min.js',
        'node_modules/flexslider/2.6.2/jquery.flexslider.js'
   ], 'public/js/all.js')
   .styles([
        'node_modules/flexslider/flexslider.css',
        'node_modules/font-awesome/css/font-awesome.min.css'
    ], 'public/css/all.css')
    .copy('node_modules/font-awesome/fonts/fontawesome-webfont.eot', 'public/fonts/fontawesome-webfont.eot')
    .copy('node_modules/font-awesome/fonts/fontawesome-webfont.svg', 'public/fonts/fontawesome-webfont.svg')
    .copy('node_modules/font-awesome/fonts/fontawesome-webfont.ttf', 'public/fonts/fontawesome-webfont.ttf')
    .copy('node_modules/font-awesome/fonts/fontawesome-webfont.woff', 'public/fonts/fontawesome-webfont.woff')
    .copy('node_modules/font-awesome/fonts/fontawesome-webfont.woff2', 'public/fonts/fontawesome-webfont.woff2')
    .copy('node_modules/font-awesome/fonts/FontAwesome.otf', 'public/fonts/FontAwesome.otf')
    .copy('node_modules/flexslider/fonts/flexslider-icon.eot', 'public/fonts/flexslider-icon.eot')
    .copy('node_modules/flexslider/fonts/flexslider-icon.svg', 'public/fonts/flexslider-icon.svg')
    .copy('node_modules/flexslider/fonts/flexslider-icon.ttf', 'public/fonts/flexslider-icon.ttf')
    .copy('node_modules/flexslider/fonts/flexslider-icon.woff', 'public/fonts/flexslider-icon.woff')
   .version();;
