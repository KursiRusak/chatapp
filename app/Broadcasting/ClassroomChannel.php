<?php

namespace App\Broadcasting;

use App\Models\User;
use App\Models\Classroom;

class ClassroomChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\Models\User  $user
     * @return array|bool
     */
    public function join(User $user, Classroom $classroom)
    {
        foreach($classroom->subscribers as $subscriber)
        {
            if($user === $subscriber){
                return [
                    'user' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                ];
            }
        }
        return false;
    }
}
