<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassroomSubscriber extends Model
{
    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
