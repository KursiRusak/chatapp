<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Classroom extends Model
{
    use Searchable, sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function mentor_user()
    {
        return $this->belongsTo(User::class, 'mentor');
    }

    public function subscribers()
    {
        return $this->hasMany(ClassroomSubscriber::class);
    }

}
