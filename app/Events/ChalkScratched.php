<?php

namespace App\Events;

use App\Models\User;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;

class ChalkScratched implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $channel;
    public $x;
    public $y;
    public $xLast;
    public $yLast;
    public $user;

    // public $broadcastQueue=
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($channel, User $user, int $x, int $y, int $xLast, int $yLast)
    {
        $this->channel = $channel;
        $this->x       = $x;
        $this->y       = $y;
        $this->xLast   = $xLast;
        $this->yLast   = $yLast;
        $this->user    = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel($this->channel);
    }

    public function onQueue()
    {
      return 'real-time-queue';
    }
}
