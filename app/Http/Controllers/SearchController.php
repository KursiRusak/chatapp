<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classroom;

class SearchController extends Controller
{

    public $photos;

    public function __construct()
    {
        $this->photos = collect([
            'library',
            'bookshelf',
            'bokeh',
            'blur',
            'scenery',
            'dawn',
            'lake',
            'trees',
            'mountain',
            'sunrise',
            'morning',
            'nature',
            'wallpaper',
            'forest',
            'autumn'
        ]);
    }

    public function index()
    {
        $classrooms = Classroom::with('mentor_user')
                                ->take(15)
                                ->orderBy('created_at', 'desc')
                                ->get();
        // dd($this->photos->where('library'));
        // dd(count($this->photos));
        return view('home')
              ->with('classrooms', $classrooms)
              ->with('photos', $this->photos);
    }

    public function search($query)
    {
        $classrooms = Classroom::search($query)->paginate(15);
        $classrooms->load('mentor_user');

        return view('home')
            ->with('query', $query)
            ->with('classrooms', $classrooms)
            ->with('photos', $this->photos);
    }

    public function lazyload($query, $skip)
    {
        if($query == 'all')
        {
            $classrooms = Classroom::with('mentor')->skip($skip * 15)->take(15)->orderBy('created_at', 'desc')->get();
        } else
        {
            $classrooms = Classroom::search($query)->skip($skip * 15)->take(15);
            $classrooms->load('mentor');
        }

        $response = ['classrooms' => $classrooms, 'photos' => $this->photos];

        return $response;
    }

    public function check($check)
    {
      if ($user_id != $Classroom_Subscriber::with('user_id'))
      {
        $check = 'true';
      }

    }

  
}
