<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\{MessagePosted, ChalkScratched, ErasedEvent};
use App\Models\{Classroom, ClassroomSubscriber, Message};
use Auth;

class ClassroomController extends Controller
{

    public $photos;

    public function __construct()
    {
        $this->photos = collect([
            'library',
            'bookshelf',
            'bokeh',
            'blur',
            'scenery',
            'dawn',
            'lake',
            'trees',
            'mountain',
            'sunrise',
            'morning',
            'nature',
            'wallpaper',
            'forest',
            'autumn'
        ]);
    }

    public function index($ch)
    {
        $classroom = Classroom::where('slug', $ch)->first();

        if($classroom->mentor == Auth::user()->id){
          $member = ClassroomSubscriber::where('classroom_id', $classroom->id)->where('approved',1)->get();
          $requestMember = ClassroomSubscriber::where('classroom_id',$classroom->id)->where('approved',0)->get();

          return view('classroom.index')
                  ->with('classroom', $classroom)
                  ->with('settings', true)
                  ->with('member',$member)
                  ->with('requestMember',$requestMember);
        }

        $user = ClassroomSubscriber::where('classroom_id', $classroom->id)->where('user_id', Auth::user()->id)->first();
        if($user){
          return view('classroom.index')
                  ->with('classroom', $classroom)
                  ->with('settings', false);
        } else {
          return redirect()
                  ->route('home');
        }
    }

    public function overview($ch)
    {
        $classroom = Classroom::where('slug', $ch)->first();
        $photo = $this->photos[rand(0, 14)];

        $sub = ClassroomSubscriber::where('classroom_id', $classroom->id)->where('user_id', Auth::user()->id)->first();
        if($sub != null ){
          if($sub->approved == 0)
            $status = 'not approved';
          else
            $status = 'subscriber';
        }
        else
          $status = 'not subscriber';

        if(Classroom::where('id', $classroom->id)->where('mentor', Auth::user()->id)->first())
          $status = 'subscriber';

        return view('classroom.overview')
                  ->with('classroom', $classroom)
                  ->with('photo', $photo)
                  ->with('status', $status);
    }

    public function requestJoin(Request $request)
    {
        $subscriber = new ClassroomSubscriber;
        $subscriber->classroom_id = (int) $request->classroom;
        $subscriber->user_id = Auth::user()->id;
        $subscriber->save();
    }

    public function approveJoin(Request $request)
    {
      $user_id = $request->input('user_id');
      $classroom = Classroom::where('slug', explode(".", $request->input('channel'))[1])->first();
      $room = ClassroomSubscriber::where('classroom_id', $classroom->id)->where('user_id',$user_id)->first();
      $room->approved = 1;
      $room->save();
    }

    public function rejectJoin(Request $request)
    {
      $user_id = $request->input('user_id');
      $classroom = Classroom::where('slug', explode(".", $request->input('channel'))[1])->first();
      $room = ClassroomSubscriber::where('classroom_id', $classroom->id)->where('user_id',$user_id)->first();
      $room->delete();
    }

    public function kickMember(Request $member)
    {
      $user_id = $member->input('user_id');
      $classroom = Classroom::where('slug', explode(".", $member->input('channel'))[1])->first();
      $room = ClassroomSubscriber::where('classroom_id',$classroom->id)->where('user_id',$user_id)->first();
      $room->delete();
    }

    public function create()
    {
        return view('classroom.create');
    }

    public function store(Request $request)
    {
        $file = $request->file('pdf');
        $classroom = new Classroom;
        $classroom->title = $request->input('title');
        $classroom->description = $request->input('description');

        if($file != null)
          $classroom->pdf = $file->getClientOriginalName();

        $classroom->mentor = Auth::user()->id;
        $classroom->save();

        $destinationPath = 'pdf';
        if($file != null)
          $file->move($destinationPath, $file->getClientOriginalName());


        // $classroomSubs = new ClassroomSubscriber;
        // $classroomSubs->classroom_id = $classroom->id;
        // $classroomSubs->user_id = Auth::user()->id;
        // $classroomSubs->save();

        return redirect()
                ->route('classroom',['ch'=>$classroom->slug]);
    }

    public function emitSliderEvent(Request $request)
    {

    }

    public function emitChatMessage(Request $request)
    {
        $channel = $request->input('channel');
        $user    = Auth::user();
        $msg     = $request->input('message');

        $message          = new Message;
        $message->user_id = $user->id;
        $message->body    = $msg;
        $message->save();

        broadcast(new MessagePosted($channel, $user, $message));

        return ['status' => 'OK'];
    }

    public function emitChalkMessage(Request $request)
    {
        $channel = $request->input('channel');
        $user    = Auth::user();
        $x = $request->input('x');
        $y = $request->input('y');
        $xLast = $request->input('xLast');
        $yLast = $request->input('yLast');

        broadcast(new ChalkScratched($channel, $user, $x, $y, $xLast, $yLast))->toOthers();

        return ['status' => 'OK'];
    }

    public function emitEraserMessage(Request $request)
    {
        $channel = $request->input('channel');
        $user    = Auth::user();
        $x = $request->input('x');
        $y = $request->input('y');
        $xLast = $request->input('xLast');
        $yLast = $request->input('yLast');

        broadcast(new ErasedEvent($channel, $user, $x, $y, $xLast, $yLast))->toOthers();

        return ['status' => 'OK'];
    }
}
