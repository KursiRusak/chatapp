<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{User, ClassroomSubscriber, Classroom};

class DashboardController extends Controller
{
  public $photos;

    public function __construct()
    {
        $this->photos = collect([
            'library',
            'bookshelf',
            'bokeh',
            'blur',
            'scenery',
            'dawn',
            'lake',
            'trees',
            'mountain',
            'sunrise',
            'morning',
            'nature',
            'wallpaper',
            'forest',
            'autumn'
        ]);
    }

    public function index($id)
    {
      $photo = $this->photos[rand(0,14)];
      $user = User::where("id", $id)->first();
      $subclassrooms = ClassroomSubscriber::with('classroom')->where('user_id', $id)->where('approved', 1)->get();
      $openclassrooms = Classroom::with('mentor_user')
                              ->orderBy('created_at', 'desc')
                              ->where('mentor', $id)
                              ->get();
      
      return view('dashboard')
        ->with('user', $user)
        ->with('subclassrooms', $subclassrooms)
        ->with('openclassrooms', $openclassrooms)
        ->with('photo', $photo)
        ->with('photos',$this->photos)
        ->with('classroom.mentor_user');
    }


}
