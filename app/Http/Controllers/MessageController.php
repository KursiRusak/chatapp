<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MessagePosted;
use App\Models\Message;
use Auth;

class MessageController extends Controller
{
    public function emitMessage(Request $request)
    {
        $channel = $request->input('channel');
        $user    = Auth::user();
        $msg = $request->input('message');

        $message          = new Message;
        $message->user_id = $user->id;
        $message->body    = $msg;
        $message->save();

        broadcast(new MessagePosted($channel, $user, $message));

        return ['status' => 'OK'];
    }
}
